/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

/**
 *
 * @author Win
 */
public abstract class Event implements Comparable {
    int eventTime; //Event time, in server time (not really used)
    protected EventType eventType; //This is used for superseding classes to indicate the event type
    
    /**
     * Create an event at the time it needs to be fired
     * @param time 
     */
    public Event(int time) {
        this.eventTime = time;
        this.eventType = EventType.None;
    }
    
    public int getTime() {
        return eventTime;
    }
    
    public EventType getType() {
        return eventType;
    }

    //We compare events in the order they are to be fired
    @Override
    public int compareTo(Object t) {
        Event evt = (Event) t;
        return evt.getTime()-this.getTime();
    }
    
    public static enum EventType {
        None,
        TerrainModify
    }
}
