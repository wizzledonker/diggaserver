/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import com.jme3.math.Vector2f;

/**
 *
 * @author Win
 */
public class TerrainModifyEvent extends Event {
    Vector2f position;
    boolean dig;
    
    public TerrainModifyEvent(int time, Vector2f position, boolean dig) {
        super(time);
        super.eventType = EventType.TerrainModify;
        
        this.position = position;
        this.dig = dig;
    }
    
    public boolean isDigging() {
        return dig;
    }
    
    public Vector2f getPosition() {
        return position;
    }
    
}
