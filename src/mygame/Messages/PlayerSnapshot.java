/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Win
 */

@Serializable
public class PlayerSnapshot extends AbstractMessage {
    float xPos;
    float yPos;
    byte rotation;
    
    int playerId;
    
    public PlayerSnapshot() {
        
    }
    
    /**
     * Designates details about the player to be sent to the world.
     * @param playerID The id of the player to update
     * @param x new X position
     * @param y new Y position
     * @param rot Rotation in radians. Will be converted to a byte between -128 and 127
     */
    public PlayerSnapshot(int playerID, float x, float y, float rot) {
        xPos = x;
        yPos = y;
        rotation = rotationFromRadians(rot); //Converts the rotation value into a number between -128 and 127 to represent rotation
        
        playerId = playerID;
    }
    
    public float getX() {
        return xPos;
    }
    
    public float getY() {
        return yPos;
    }
    
    public int getId() {
        return playerId;
    }
    
    /**
     * Gets the rotation amount in radians
     * @return rotation, in radians
     */
    public float getRotation() {
        return rotationToRadians(rotation);
    }
    
    /**
     * Converts a radian between 0 and 2*PI to a byte to send over the network.
     * @param rot Rotation in radians
     * @return Rotation in bytes
     */
    public static byte rotationFromRadians(float rot) {
        return (byte) (((rot/(2f*FastMath.PI))%1f) * 255 - 128);
    }
    
    /**
     * Converts a byte sent over the network back to radians.
     * @param rot Rotation in bytes
     * @return Rotation float in radians
     */
    public static float rotationToRadians(byte rot) {
        return ((rot+128f)/255f)*(2f*FastMath.PI);
    }
}
