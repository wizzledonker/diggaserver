/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * Broadcase this message to all connected players so they can update their player list
 */
@Serializable
public class PlayerConnectMessage extends AbstractMessage {
    int connectServerTime;
    String playerName;
    int playerId;
    
    public PlayerConnectMessage() {
        
    }
    
    public PlayerConnectMessage(String name, int connectTime, int playerId) {
        this.playerName = name;
        this.connectServerTime = connectTime;
        this.playerId = playerId;
    }
    
    public int getPlayerId() {
        return playerId;
    }
    
    public String getPlayerName() {
        return playerName;
    }
}
