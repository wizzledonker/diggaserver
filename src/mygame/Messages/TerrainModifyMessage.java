/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Win
 */
@Serializable
public class TerrainModifyMessage extends AbstractMessage {
    boolean dig;
    float xPos;
    float yPos;
    
    public TerrainModifyMessage() {
        
    }
    
    public TerrainModifyMessage(float x, float y, boolean digOrRaise) {
        this.xPos = x;
        this.yPos = y;
        this.dig = digOrRaise;
    }
    
    public float getX() {
        return xPos;
    }
    
    public float getY() {
        return yPos;
    }
    
    public boolean isDig() {
        return dig;
    }
}
