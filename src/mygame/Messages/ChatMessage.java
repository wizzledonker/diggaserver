/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Win
 */
@Serializable
public class ChatMessage extends AbstractMessage {

    private String name;
    private String message;

    public ChatMessage() {
    }

    public ChatMessage(String name, String message) {
        setName(name);
        setMessage(message);
    }
    
    public String getMessage() {
        return message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMessage(String s) {
        this.message = s;
    }

    @Override
    public String toString() {
        return name + ":" + message;
    }
}
