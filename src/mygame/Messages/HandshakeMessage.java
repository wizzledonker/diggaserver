/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Allows the player to send some information to the server when connecting
 * @author Win
 */
@Serializable
public class HandshakeMessage extends AbstractMessage {
    String playerName;
    
    public HandshakeMessage() {
        
    }
    
    public HandshakeMessage(String name) {
        this.playerName = name;
    }
    
    public String getName() {
        return playerName;
    }
}
