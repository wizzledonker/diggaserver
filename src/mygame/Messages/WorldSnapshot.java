/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import Entities.Player;
import Events.Event;
import Events.Event.EventType;
import Events.TerrainModifyEvent;
import com.jme3.math.Vector2f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Win
 */
@Serializable
public class WorldSnapshot extends AbstractMessage {
    float[] positionsX;
    float[] positionsY;
    byte[] rotations;
    
    int[] playerIds;
    
    int numPlayers;
    
    int worldTime;
    int tick;
    
    //Event processing information, only allocated as needed. There's an event x and event y
    EventType event; //The type of the event
    float eventX; //Value common to most events
    float eventY; //Value common to most events
    boolean eventStatus; //Value common to most events
    
    public WorldSnapshot() {
        
    }
    
    public WorldSnapshot(Set<Player> playerPositions, int time, int lastTick, Event ev) {
        numPlayers = playerPositions.size();
        
        positionsX = new float[numPlayers];
        positionsY = new float[numPlayers];
        rotations = new byte[numPlayers];
        
        playerIds = new int[numPlayers];
        
        int currPos = 0;
        for (Player p : playerPositions) {
            playerIds[currPos] = p.getClientId();
            positionsX[currPos] = p.getPosition().getX();
            positionsY[currPos] = p.getPosition().getY();
            rotations[currPos] = PlayerSnapshot.rotationFromRadians(p.getRotation());
            
            currPos++;
        }
        
        this.worldTime = time;
        this.tick = lastTick;
        
        //If the event is null, we just set the event type to none
        if (ev == null) {
            event = EventType.None;
        } else {
            //Set the event up depending on the type
            if (ev.getType() == EventType.TerrainModify) {
                TerrainModifyEvent tme = (TerrainModifyEvent) ev;
                event = tme.getType();
                eventX = tme.getPosition().getX();
                eventY = tme.getPosition().getY();
                eventStatus = tme.isDigging();
            }
        }
    }
    
    public Event getEvent() {
        if (event == EventType.TerrainModify) {
            return new TerrainModifyEvent(worldTime, new Vector2f(eventX, eventY), eventStatus);
        }
        
        return null;
    }
    
    public Set<Player> getPlayers() {
        Set<Player> players = new HashSet();
        for (int i = 0; i < numPlayers; i++) {
            players.add(new Player(new Vector2f(positionsX[i], positionsY[i]), PlayerSnapshot.rotationToRadians(rotations[i]), 0, i, playerIds[i], null));
        }
        return players;
    }
    
    /**
     * Gets the server time in milliseconds (time since the server started)
     * @return Time since server started
     */
    public int getServerTime() {
        return worldTime;
    }
    
    /**
     * Gets the time the last tick took on the server
     * @return Tick Time
     */
    public int getTicktime() {
        return tick;
    }
}
