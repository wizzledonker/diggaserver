/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.math.FastMath;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import java.util.ArrayList;

/**
 *
 * @author Win
 */

@Serializable
public class TerrainMessage extends AbstractMessage {
    private int heightmapSize;
    private byte[] heightmapMessage;
    private int verticalSize;
    
    //Chunk numbers
    int currentChunk;
    
    public TerrainMessage() {
        
    }
    
    public TerrainMessage(int size, AbstractHeightMap hMap, int vertical, int currChunk) {
        
        //Normalize the terrain so we store stuff perfectly in the heightmap
        hMap.normalizeTerrain(127f);
        float[] map = hMap.getScaledHeightMap();
        
        int chunkSize = 128*128; //The number of floats in a single chunk
        
        //Convert the float heightmap to an array of bytes for compression
        byte[] heightmapBytes = new byte[chunkSize];
        for (int i = 0; i < chunkSize; i++) {
            heightmapBytes[i] = (byte) FastMath.floor(map[i + chunkSize*currChunk]);
        }
        
        //Add the bytes to the terrain
        heightmapMessage = heightmapBytes;
        heightmapSize = size;
        verticalSize = vertical;
        currentChunk = currChunk;
    }
    
    /**
     * The height of everything is divisible by this number
     * @return the size of a single step
     */
    public float getStepSize() {
        return ((float) verticalSize/127f);
    }
    
    public int getCurrentChunk() {
        return currentChunk;
    }
    
    public int getTotalChunks() {
        return (heightmapSize/128)*(heightmapSize/128);
    }
    
    public float[] getHeightMap() {
        float[] heightMapReturn = new float[heightmapMessage.length];
        for (int i = 0; i < heightmapMessage.length; i++) {
            heightMapReturn[i] = ((float) heightmapMessage[i]) * getStepSize(); //Transform back to the original height
        }
        return heightMapReturn;
    }
    
    public int getHeightMapSize() {
        return heightmapSize;
    }
}
