/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Win
 */
@Serializable
public class PlayerDisconnectMessage extends AbstractMessage{
    int playerId;
    
    public PlayerDisconnectMessage() {
        
    }
    
    //Removes a player from the game with ID (Broadcast to the whole server)
    public PlayerDisconnectMessage(int id) {
        playerId = id;
    }
    
    public int getId() {
        return playerId;
    }
}
