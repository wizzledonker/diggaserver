/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author Win
 */
public class ServerUpdateThread implements Runnable {
    
    boolean serverQuit;
    final WorldManager instance;
    
    public ServerUpdateThread(final WorldManager serv) {
        serverQuit = false;
        instance = serv;
    }
    
    public void quitGracefully() {
        serverQuit = true;
    }

    @Override
    public void run() {
        while (!serverQuit) {
            if (instance != null) {
                //Run the server loop
                //instance.log(null, "Test");
                instance.updateLoop();
            }
        }
        System.out.println("World update thread stopped.");
    }
    
}
