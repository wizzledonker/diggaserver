/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 *
 * @author Win
 */
public class DiggaServerUserInterface extends JFrame {
    
    private final JEditorPane messageLog;
    private final JTextField messageField;
    private StringBuilder chatMessages = new StringBuilder();
    
    private final DiggaServer serverInstance;
    
    public DiggaServerUserInterface(final DiggaServer instance) {
        serverInstance = instance;
        
        //Start the user interface to log what's going on
        // Build out the UI
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(800, 600);

        //Message history
        messageLog = new JEditorPane();
        messageLog.setEditable(false);
        messageLog.setContentType("text/html");
        messageLog.setText("<html><body>");
        
        getContentPane().add(new JScrollPane(messageLog), "Center");
        
        // A way for the server manager to input commands     
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
        messageField = new JTextField();
        p.add(messageField);
        JButton b = new JButton(new AbstractAction("Send Command") {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //Really simply, this performs a command as a super user
                serverInstance.runCommand(null, "Server", messageField.getText());
            }
            
        });
        p.add(b);
        
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                instance.close();
            }
        });
        
        //Add all the stuff we've now added
        getContentPane().add(p, "South");
        
        this.getRootPane().setDefaultButton(b);
    }
    
    public void log(String message) {
        chatMessages.append(message);
        messageLog.setText(chatMessages.toString());
    }
}
