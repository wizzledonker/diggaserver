/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import Entities.Entity;
import Entities.Player;
import Events.Event;
import Events.TerrainModifyEvent;
import com.jme3.math.Vector2f;
import com.jme3.network.HostedConnection;
import com.jme3.network.Server;
import com.jme3.network.service.HostedService;
import com.jme3.network.service.HostedServiceManager;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import mygame.Messages.WorldSnapshot;

/**
 * Manages everything about the world as the server sees it
 * @author Win
 */
public class WorldManager implements HostedService {

    DiggaServer mainServer;
    ServerUpdateThread mainThread;
    Thread updateThread;
    
    //Variables to keep the world time
    private float TPS = 15f; //15 ticks per second by default
    private long serverStartTime;
    private int lastServerTime;
    private int serverTime;
    private int tickNumber;
    
    //Finally, the list of entities in the world
    private int entityCounter = 0;
    private ConcurrentSkipListSet<Entity> worldEntities;
    private ConcurrentSkipListSet<Event> worldEvents;
    
    public WorldManager(DiggaServer instance) {
        this.mainServer = instance;
        this.mainThread = new ServerUpdateThread(this);
        
        serverStartTime = System.currentTimeMillis();
        lastServerTime = 0;
        tickNumber = 0;
        serverTime = 0;
        
        updateThread = new Thread(mainThread);
        
        worldEntities = new ConcurrentSkipListSet();
        worldEvents = new ConcurrentSkipListSet();
    }
    
    /**
     * Should only be called from the ServerUpdateThread instance.
     */
    public void updateLoop() {
        //Get the time since the server started
        this.serverTime = (int) (System.currentTimeMillis() - this.serverStartTime);
        if (this.serverTime > (this.lastServerTime + 1000/TPS)) {
            
            //Perform the actions we've got to perform for this tick (this is stuff that happens 20 times a second, world updates)
            
            Set<Player> playerData = new HashSet();
            
            //Send all the players an update of the world snapshot (first checking if a player has joined or left)
            for (Entity ent : worldEntities) {
                if (ent instanceof Player) {
                    Player entP = (Player) ent;
                    
                    playerData.add(entP);
                }
            }
            
            Event e = null;
            if (!worldEvents.isEmpty()) {
                if (worldEvents.first().getTime() < this.getWorldTime()) {
                    e = worldEvents.first();
                    worldEvents.remove(e);
                }
            }
            
            //Send using UDP, the last parameter is how long the tick took so the client can interpolate accurately
            //To simplify event bridging, we only send one event per server tick
            WorldSnapshot snap = new WorldSnapshot(playerData, this.serverTime, this.serverTime-this.lastServerTime, e);
            snap.setReliable(false);

            mainServer.getServer().broadcast(snap);
            
            this.lastServerTime = this.serverTime;
            this.tickNumber++;
            //mainServer.getServer().broadcast(new WorldSnapshot(playerLocations, this.serverTime));
        }
    }
    
    /**
     * Add an event to the world. Executes at the server time specified in the event.
     * @param e 
     */
    public void addEvent(Event e) {
        this.worldEvents.add(e);
    }
    
    public void modifyTerrain(float x, float y, boolean dig) {
        
        int x2 = Math.round(x)/2+mainServer.getLevelSize()/2;
        int y2 = Math.round(y)/2+mainServer.getLevelSize()/2;
        
        //First, we change the terrain to update it for new clients (using the average location given to us)
        mainServer.currentLevel.setHeightAtPoint((mainServer.currentLevel.getTrueHeightAtPoint(x2, y2)+(dig?-1f:1f)), x2, y2);
        
        //Add an event, at the current world time so it executes now rather than later
        addEvent(new TerrainModifyEvent(getWorldTime(), new Vector2f(x, y), dig));
    }
    
    /**
     * Add a player to the world with an ID and name. Also begins listening for PlayerSnapshot messages under that name
     * @param id Player ID
     * @param name Player name
     */
    public void addPlayer(int id, String name) {
        worldEntities.add(new Player(Vector2f.ZERO, 0f, this.serverTime, entityCounter++, id, name));
    }
    
    /**
     * Returns true if a player with a name is already in the world.
     * @param name The player name to check
     * @return Whether the player is on the server already
     */
    public boolean hasPlayer(String name) {
        for (Entity ent : worldEntities) {
            if (ent instanceof Player) {
                if (((Player) ent).getName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void removePlayer(int id) {
        for (Entity ent : worldEntities) {
            if (ent instanceof Player) {
                //Remove if it's the right id
                if (((Player) ent).getClientId() == id) {
                    worldEntities.remove(ent);
                }
            }
        }
    }
    
    /**
     * Updates the x and y coordinates for a player with an ID
     * @param id The player ID to update
     * @param x new X position
     * @param y new Y position
     */
    public void updatePlayerPosition(int id, float x, float y) {
        for (Entity ent: worldEntities) {
            if (ent instanceof Player) {
                if (((Player) ent).getClientId() == id) {
                    ent.updatePosition(new Vector2f(x, y));
                }
            }
        }
    }
    
    public void updatePlayerRotation(int id, float rot) {
        for (Entity ent: worldEntities) {
            if (ent instanceof Player) {
                if (((Player) ent).getClientId() == id) {
                    ent.updateRotation(rot);
                }
            }
        }
    }
    
    public int getWorldTime() {
        return serverTime;
    }
    
    /**
     * Set the server tickrate
     * @param tps 
     */
    public void setTPS(float tps) {
        this.TPS = tps;
    }
    
    @Override
    public void initialize(HostedServiceManager serviceManager) {
        
    }

    @Override
    public void start() {
        //Operations performed after we start
        updateThread.start();
    }

    @Override
    public void stop() {
        try {
            mainThread.quitGracefully();
            System.out.println("Joining world update thread...");
            updateThread.join(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WorldManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void terminate(HostedServiceManager serviceManager) {
    }

    @Override
    public void connectionAdded(Server server, HostedConnection conn) {
        
    }

    @Override
    public void connectionRemoved(Server server, HostedConnection conn) {
        
    }
    
}
