package mygame;

import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.network.serializing.Serializer;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.HillHeightMap;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import mygame.Messages.ChatMessage;
import mygame.Messages.HandshakeMessage;
import mygame.Messages.PlayerConnectMessage;
import mygame.Messages.PlayerDisconnectMessage;
import mygame.Messages.PlayerSnapshot;
import mygame.Messages.TerrainMessage;
import mygame.Messages.TerrainModifyMessage;
import mygame.Messages.WorldSnapshot;

/**
 * This is the server file for Digga. It's responsible for running a Digga server, and Digga clients can connect to it freely.
 * Move your Logic into AppStates or Controls
 * @author wizzledonker
 */
public class DiggaServer {
    
    public static final String NAME = "Digga"; //Will later be configurable
    public static final int VERSION = 1;
    public static final int PORT = 5110;
    public static final int UDP_PORT = 5110;
    
    DiggaServerUserInterface ui;
    WorldManager world;

    private Server server;
    private boolean isRunning;
    
    protected AbstractHeightMap currentLevel;
    protected int levelSize = 512;
    
    public DiggaServer() throws IOException {

        // This basically ensures that the client and the server are running the same version
        this.server = Network.createServer(NAME, VERSION, PORT, UDP_PORT);

        // Initialize our own messages only after the server has been created.
        // It registers some additional messages with the serializer by default
        // that need to go before custom messages.
        initializeClasses();

        OperationHandler handler = new OperationHandler();
        server.addMessageListener(handler, ChatMessage.class);
        server.addMessageListener(handler, HandshakeMessage.class);
        server.addMessageListener(handler, PlayerSnapshot.class);
        server.addMessageListener(handler, TerrainModifyMessage.class);
        
        world = new WorldManager(this);
        server.getServices().addService(world); //Adds the hosted service that updates the player locations
        
        server.addConnectionListener(new PlayerConnectionListener());
    }
    
    
    public boolean isRunning() {
        return isRunning;
    }
    
    public synchronized void start() {
        if( isRunning ) {
            return;
        }
        server.start();
        isRunning = true;
    }
    
    public Server getServer() {
        return this.server;
    }
    
    public synchronized void close() {
        if( !isRunning ) {
            return;
        }
        
        // Gracefully let any connections know that the server is
        // going down.  Without this, their connections will simply
        // error out.
        for( HostedConnection conn : server.getConnections() ) {
            conn.close("Server is shutting down.");
        }
        try {
            Thread.sleep(1000); // wait a couple beats to let the messages go out
        } catch( InterruptedException e ) {
            e.printStackTrace();
        }
        
        server.close();        
        isRunning = false;
        notifyAll();
        ui.dispose();
    }
    
    public void log(HostedConnection conn, String message) {
        if (conn == null) {
            //It's a server log
            if (ui != null) {
                ui.log("<b>[" + String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(world.getWorldTime())) + ":" +String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(world.getWorldTime())%60) + "]</b> " + message + "<br />");
            }
        } else {
            conn.send(new ChatMessage("server", message));
        }
    }
    
    protected void runCommand( HostedConnection conn, String user, String command ) {
        if( "/shutdown".equals(command) ) {
            log(conn, "Server shutting down...");
            close();
        } else if( "/help".equals(command) ) {
            StringBuilder sb = new StringBuilder();
            sb.append("Chat commands:<br />");
            sb.append("/help - prints this message.<br />");
            sb.append("/shutdown - shuts down the server.<br />");
            log(conn, sb.toString());   
        }
    } 

    public static void initializeClasses() {
        // Doing it here means that the client code only needs to
        // call our initialize. 
        Serializer.registerClass(ChatMessage.class);
        Serializer.registerClass(TerrainMessage.class);
        Serializer.registerClass(HandshakeMessage.class);
        Serializer.registerClass(TerrainModifyMessage.class);
        Serializer.registerClass(PlayerConnectMessage.class);
        Serializer.registerClass(PlayerDisconnectMessage.class);
        Serializer.registerClass(PlayerSnapshot.class);
        Serializer.registerClass(WorldSnapshot.class);
    }

    public static void main(String... args) throws Exception {
 
        // Increate the logging level for networking...
        /*System.out.println("Setting logging to max");
        Logger networkLog = Logger.getLogger("com.jme3.network"); 
        networkLog.setLevel(Level.FINEST);
 
        // And we have to tell JUL's handler also   
        // turn up logging in a very convoluted way
        Logger rootLog = Logger.getLogger("");
        if( rootLog.getHandlers().length > 0 ) {
            rootLog.getHandlers()[0].setLevel(Level.FINEST);
        }     */   
    
        DiggaServer diggaServer = new DiggaServer();
        
        diggaServer.start();
        
        diggaServer.ui = new DiggaServerUserInterface(diggaServer);
        diggaServer.ui.setVisible(true);
 
        diggaServer.log(null, "Waiting for connections on port:" + PORT);
        diggaServer.log(null, "Generating terrain...");
        diggaServer.generateTerrain();
        diggaServer.log(null, "Terrain Ready.");
                
        diggaServer.log(null, "Done. Accepting players.");
        // Keep running basically forever
        while( diggaServer.isRunning ) {
            synchronized (diggaServer) {
                diggaServer.wait();
            }
        }
    }
    
    private HillHeightMap islandTerrain(HillHeightMap map) {
        for (int x = 0; x < map.getSize(); x++) {
            for (int z = 0; z < map.getSize(); z++) {
                float middle = map.getSize()/2f;
                float scaleFac = FastMath.pow(1f-(new Vector2f(x, z)).distance(new Vector2f(middle, middle))/(middle*1.5f), 2);
                map.setHeightAtPoint(map.getTrueHeightAtPoint(x, z)*scaleFac, x, z);
            }
        }
        return map;
    }
    
    public int getLevelSize() {
        return this.levelSize;
    }
    
    public void generateTerrain() {
        
        //Generation algorithm for our heightmap goes here
        try {
            currentLevel = islandTerrain(new HillHeightMap(levelSize, 4096, 4, 32));
            currentLevel.normalizeTerrain(127f);
            //currentLevel.erodeTerrain();
        } catch (Exception ex) {
            System.out.println("Exception thrown when generating terrain!");
            ex.printStackTrace();
        }
    }
    
    public void sendTerrain(HostedConnection con) {
        for (int i = 0; i < (currentLevel.getSize()/128)*(currentLevel.getSize()/128); i++) {
            log(null, "Sending chunk " + i);
            TerrainMessage m = new TerrainMessage(currentLevel.getSize(), currentLevel, 64, i);
            m.setReliable(true); //use TCP to test recieval
            con.send(m);
        }
    }

    private class OperationHandler implements MessageListener<HostedConnection> {

        public OperationHandler() {
        }

        @Override
        public void messageReceived(HostedConnection source, Message m) {
            if (m instanceof ChatMessage) {
                // Keep track of the name just in case we 
                // want to know it for some other reason later and it's
                // a good example of session data
                ChatMessage cm = (ChatMessage)m;
                source.setAttribute("name", cm.getName());

                // Check for a / command
                if( cm.getMessage().startsWith("/") ) {
                    runCommand(source, cm.getName(), cm.getMessage());
                    return;
                }

                log(null, "Broadcasting:" + m + "  reliable:" + m.isReliable());

                // Just rebroadcast... the reliable flag will stay the
                // same so if it came in on UDP it will go out on that too
                source.getServer().broadcast(cm);
            } else if (m instanceof HandshakeMessage) {
                HandshakeMessage hm = (HandshakeMessage) m;
                
                log(null, "Client handshake completed. Player name: " + hm.getName() + " ID: " + source.getId());
                
                //Check if the player is already in the world, and if so kick them
                if (world.hasPlayer(hm.getName())) {
                    source.close("Nickname already in use!");
                    return;
                }
                //Broadcast a new player connect message to the server, passing the server time that the player connected
                source.getServer().broadcast(new PlayerConnectMessage(hm.getName(), world.getWorldTime(), source.getId()));
                
                //Add the player to the world
                world.addPlayer(source.getId(), hm.getName());
            } else if (m instanceof PlayerSnapshot) {
                PlayerSnapshot playerMsg = (PlayerSnapshot) m;
                
                world.updatePlayerPosition(playerMsg.getId(), playerMsg.getX(), playerMsg.getY());
                world.updatePlayerRotation(playerMsg.getId(), playerMsg.getRotation());
            } else if (m instanceof TerrainModifyMessage) {
                TerrainModifyMessage terrMsg = (TerrainModifyMessage) m;
                
                //log(null, "Terrain modify message recieved");
                world.modifyTerrain(terrMsg.getX(), terrMsg.getY(), terrMsg.isDig());
            } else {
                System.err.println("Received odd message:" + m);
            }
        }
    }

    private class PlayerConnectionListener implements ConnectionListener {

        @Override
        public void connectionAdded( Server server, HostedConnection conn ) {

            log(null, "Player Connected: " + conn);
            
            log(null, "Sending terrain to player " + conn.getId());
            sendTerrain(conn);
            
            //After we've send the terrain, we're gonna wait for a hanshake message with the player's name
        }

        @Override
        public void connectionRemoved(Server server, HostedConnection conn) {
            log(null, "Player Disconnected: " + conn);
            server.broadcast(new PlayerDisconnectMessage(conn.getId()));
            world.removePlayer(conn.getId());
        }
    }
}
