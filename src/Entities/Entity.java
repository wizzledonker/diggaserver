
package Entities;

import com.jme3.math.Vector2f;
import com.jme3.network.serializing.Serializable;
import java.lang.annotation.Annotation;

/**
 * Base entity class. They only have an x and a z parameter for positioning, as we assume that objects can't float in the sky
 * @author Win
 */
public class Entity implements Comparable {
    Vector2f currentPosition;
    float rotation;
    
    int connectTime;
    int id;
    
    /**
     * Initialise the entity with an ID. The entity ID is not the same as a playerID. Will be implemented later.
     * @param currentPosition the position to initialise the entity at
     * @param rotation The rotation of the entity between zero and 2*PI
     * @param connectTime The time the entity connected to the server
     * @param id The id of the entity in HostedConnection
    */
    public Entity(Vector2f currentPosition, float rotation, int connectTime, int id) {
        this.currentPosition = currentPosition;
        this.connectTime = connectTime;
        this.id = id;
        this.rotation = rotation;
    }
    
    public void updatePosition(Vector2f pos) {
        this.currentPosition = pos;
    }
    
    public void updateRotation(float rot) {
        this.rotation = rot;
    }
    
    public float getRotation() {
        return rotation;
    }
    
    public Vector2f getPosition() {
        return this.currentPosition;
    }
    
    public int getId() {
        return this.id;
    }

    @Override
    public int compareTo(Object t) {
        Entity p = (Entity) t;
        if (p.getId() < this.getId()) {
            return -1;
        } else if (p.getId() > this.getId()) {
            return 1;
        }
        return 0;
    }
}
