/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import com.jme3.math.Vector2f;

/**
 *
 * @author Win
 */
public class Player extends Entity {
    int clientId;
    String playerName;
    
    /**
     * Player definition for the server. In a message, players are stored in a simplified manner. In this class, they're directly
     * accessible by the client
     * @param currentPosition Vector position of the player
     * @param rotation Float representing player rotation (converted to byte in the message)
     * @param connectTime Time the player connected to the server
     * @param id The player's entity ID
     * @param clientId The player's HostedConnection id
     * @param name A string representing the player name (not necessary for the worldsnapshot message)
     */
    public Player(Vector2f currentPosition, float rotation, int connectTime, int id, int clientId, String name) {
        super(currentPosition, rotation, connectTime, id);
        this.clientId = clientId;
        this.playerName = name;
    }
    
    public void setName(String name) {
        this.playerName = name;
    }
    
    public int getClientId() {
        return clientId;
    }
    
    public String getName() {
        return this.playerName;
    }
    
}
